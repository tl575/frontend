
var UserID = window.location.href.split('userID=').pop()

  // set the focus to the input box
  // document.getElementById("wisdom").focus();

  // Initialize the Amazon Cognito credentials provider
  AWS.config.region = 'us-east-1'; // Region
  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
  // Provide your Pool Id here
  // for production: do not
    IdentityPoolId: 'us-east-1:31b536be-dad4-4a62-9f2d-7036a51e87a4',
  });

  var lexruntime = new AWS.LexRuntime();
  var lexUserId = 'chatbot-demo' + Date.now();
  var sessionAttributes = {};

  function pushChat() {

    // if there is text to be sent...
    var wisdomText = document.getElementById('wisdom');
    if (wisdomText && wisdomText.value && wisdomText.value.trim().length > 0) {

      // disable input to show we're sending it
      var wisdom = wisdomText.value.trim();
      wisdomText.value = '...';
      wisdomText.locked = true;

      // send it to the Lex runtime
      var params = {
        botAlias: '$LATEST',
        botName: 'HelpDeskBot',
        inputText: wisdom,
        userId: lexUserId,
        sessionAttributes: sessionAttributes
      };
      showRequest(wisdom);
      lexruntime.postText(params, function(err, data) {
        if (err) {
          console.log(err, err.stack);
          showError('Error:  ' + err.message + ' (see console for details)')
        }
        if (data) {
          // capture the sessionAttributes for the next cycle
          sessionAttributes = data.sessionAttributes;
          // show response and/or error/dialog status
          showResponse(data);
        }
        // re-enable input
        wisdomText.value = '';
        wisdomText.locked = false;
      });
    }
    // we always cancel form submission
    console.log('fffff');
    return false;
  }

  function showRequest(daText) {

    var conversationDiv = document.getElementById('conversation');
    var requestPara = document.createElement("P");
    requestPara.className = 'userRequest';
    requestPara.appendChild(document.createTextNode(daText));
    conversationDiv.appendChild(requestPara);
    conversationDiv.scrollTop = conversationDiv.scrollHeight;
  }

  function showError(daText) {

    var conversationDiv = document.getElementById('conversation');
    var errorPara = document.createElement("P");
    errorPara.className = 'lexError';
    errorPara.appendChild(document.createTextNode(daText));
    conversationDiv.appendChild(errorPara);
    conversationDiv.scrollTop = conversationDiv.scrollHeight;
  }

  function showResponse(lexResponse) {

    var conversationDiv = document.getElementById('conversation');
    var responsePara = document.createElement("P");
    responsePara.className = 'lexResponse';
    if (lexResponse.message) {
      responsePara.appendChild(document.createTextNode(lexResponse.message));
      responsePara.appendChild(document.createElement('br'));
    }
    if (lexResponse.dialogState === 'ReadyForFulfillment') {
      responsePara.appendChild(document.createTextNode(
        'Ready for fulfillment'));
      // TODO:  show slot values
    } else {
      responsePara.appendChild(document.createTextNode(
        '(' + lexResponse.dialogState + ')'));
    }
    conversationDiv.appendChild(responsePara);
    conversationDiv.scrollTop = conversationDiv.scrollHeight;
  }

  function Tutorial1() {
    var x = document.getElementById('arrow1');
    var y = document.getElementById('fullscreen1');
    var a = document.getElementById('conversation');
    var b = document.getElementById('chatform');

    if (x.style.display === 'none') {
      x.style.display = 'block';
      y.style.display = 'block';
    }
    else {
      x.style.display = 'none';
      y.style.display = 'none';
    }
    a.style.display = 'none';
    b.style.display = 'none';
  }


  function Tutorial2(){
    var x = document.getElementById('arrow2');
    var y = document.getElementById('fullscreen1');
    var z = document.getElementById('arrow1');
    var a = document.getElementById('fullscreen2');
    var b = document.getElementById('conversation');
    var c = document.getElementById('chatform');
    if (x.style.display === 'none') {
      x.style.display = 'block';
    }
    else {
      x.style.display = 'none';
    }
    z.style.display = 'none';
    y.style.display = 'none';
    a.style.display = 'block';
    b.style.display = 'block';
    c.style.display = 'block';
  }

  function Tutorial3(){
    var a = document.getElementById('arrow2');
    var b = document.getElementById('fullscreen1');
    var c = document.getElementById('arrow1');
    var d = document.getElementById('fullscreen2');
    a.style.display = 'none';
    b.style.display = 'none';
    c.style.display = 'none';
    d.style.display = 'none';
  }


  function toggle_chat() {
    var x = document.getElementById('conversation');
    var y = document.getElementById('chatform');

    if (x.style.display === 'none') {
      x.style.display = 'block';
      y.style.display = 'block';
    }
    else {
      x.style.display = 'none';
      y.style.display = 'none';
    }
  }

  function hide_chat() {
    var x = document.getElementById('conversation');
    var y = document.getElementById('chatform');
    var a = document.getElementById('arrow2');
    var b = document.getElementById('fullscreen1');
    var c = document.getElementById('arrow1');
    var d = document.getElementById('fullscreen2');
    // x.style.display = 'none';
    // y.style.display = 'none';
    // a.style.display = 'none';
    // b.style.display = 'none';
    // c.style.display = 'none';
    // d.style.display = 'none';
  }


  $('.hide-chat-box').click(function(){
    console.log('hi');
        $('.chat-content').slideToggle();
});
